#include <glad/glad.h> // make sure to include glad before GLFW
#include <GLFW/glfw3.h>

#include <iostream>

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow* window);

const unsigned int SCR_WIDTH = 700;
const unsigned int SCR_HEIGHT = 500;

int main() {
    /* 
      init GLFW and OpenGL version and profile

      find more info here: https://learnopengl.com/Getting-started/Hello-Window
      and GLFW docs: https://www.glfw.org/docs/latest/window.html#window_hints
    */
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    #ifdef __APPLE__
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // for MacOS
    #endif

    /* try creating window */
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "learnopengl: Hello Window", NULL, NULL);
    if (window == NULL) {
        std::cout << "GLFW: Failed to create window. \n" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    
    /* init glad */
    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    //set viewport size adjust callback
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    // keep opengl window running
    while (!glfwWindowShouldClose(window)) {

        processInput(window);

        glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        glfwSwapBuffers(window); // swap front and back buffers used during this render
        glfwPollEvents(); // check for events created by keyboard or mouse
    }

    // release all assets
    glfwTerminate();
    return 0;
}

/* Callback function to be called by GLFW on window resize and update OpenGL Viewport */
void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

/* 
    Check user input
    check if user clicked ESC and if yes close the window.
 */
void processInput(GLFWwindow* window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}
