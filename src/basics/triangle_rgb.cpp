// clang-format off
#include <glad/glad.h> // make sure to include glad before GLFW
#include <GLFW/glfw3.h>
// clang-format on

#include <iostream>

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void processInput(GLFWwindow* window);

const unsigned int SCR_WIDTH = 700;
const unsigned int SCR_HEIGHT = 500;

const char *vertexShaderSource = "#version 330 core\n"
    "layout (location = 0) in vec3 aPos;\n"
    "layout (location = 1) in vec3 aColor;\n"
    "out vec3 color;\n"
    "void main() {\n"
    "   color = aColor;\n"
    "   gl_Position = vec4(aPos.xyz, 1.0);\n"
    "}\0";

const char *fragmentShaderSource = "#version 330 core\n"
    "in vec3 color;\n"
    "out vec4 frag_color;\n"
    "void main() {\n"
    "   frag_color = vec4(color, 1.0f);\n"
    "}\0";

int main() {
    // ============================================================= GLFW ========================== 
    // init GLFW and OpenGL version and profile
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    #ifdef __APPLE__
        glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // for MacOS
    #endif

    // try creating window
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT, "learnopengl: Triangle RGB", NULL, NULL);
    if (window == NULL) {
        std::cout << "GLFW: Failed to create window. \n" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    // set viewport size adjust callback
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

    // ================================================ GLAD =======================================
    // init glad
    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    // ============================================ Compile Shader =====================================
    unsigned int vertexShader = glCreateShader(GL_VERTEX_SHADER);
    unsigned int fragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
    unsigned int shaderProgram = glCreateProgram();

    int success;

    glShaderSource(vertexShader, 1, &vertexShaderSource, NULL);
    glCompileShader(vertexShader);
    glGetShaderiv(vertexShader, GL_COMPILE_STATUS, &success);
    if (!success) {
        int logLen = 0;
        glGetShaderiv(vertexShader, GL_INFO_LOG_LENGTH, &logLen);
        char infoLog[logLen];

        glGetShaderInfoLog(vertexShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;

        glDeleteShader(vertexShader);
    }
    
    glShaderSource(fragmentShader, 1, &fragmentShaderSource, NULL);
    glCompileShader(fragmentShader);
    glGetShaderiv(fragmentShader, GL_COMPILE_STATUS, &success);
    if (!success) {
        int logLen = 0;
        glGetShaderiv(fragmentShader, GL_INFO_LOG_LENGTH, &logLen);
        char infoLog[logLen];

        glGetShaderInfoLog(fragmentShader, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER::FRAGMENT::COMPILATION_FAILED\n" << infoLog << std::endl;

        glDeleteShader(fragmentShader);
    }

    glAttachShader(shaderProgram, vertexShader);
    glAttachShader(shaderProgram, fragmentShader);
    glLinkProgram(shaderProgram);
    glGetProgramiv(shaderProgram, GL_COMPILE_STATUS, &success);
    if (!success) {
        int logLen = 0;
        glGetProgramiv(shaderProgram, GL_INFO_LOG_LENGTH, &logLen);
        char infoLog[logLen];

        glGetProgramInfoLog(shaderProgram, 512, NULL, infoLog);
        std::cout << "ERROR::SHADER_PROGRAM::LINKING_FAILED\n" << infoLog << std::endl;
    }

    // shader objects are no longer needed since they're already binded with shaderProgram
    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    // ================================== Vertices and VertexBuffers ================================
    float vertices[] = {
        // positions        // color
        -0.5f, -0.5f, 0.0f, 1.0f, 0.0f,  0.0f, // bottom-left
         0.5f, -0.5f, 0.0f, 0.0f, 1.0f,  0.0f, // bottom-right
         0.0f,  0.5f, 0.0f, 0.0f, 0.0f,  1.0f, // top
    };  

    // create, bind and copy data to vertex buffer 
    unsigned int VBO, VAO;
    
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);

    glGenBuffers(1, &VBO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)0);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void*)(3 * sizeof(float)));
    glEnableVertexAttribArray(1);
    
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);


    // ================================== Render Loop =================================================
    // keep opengl window running
    while (!glfwWindowShouldClose(window)) {

        processInput(window);

        glClearColor(0.2f, 0.2f, 0.2f, 1.0f);
        glClear(GL_COLOR_BUFFER_BIT);

        glUseProgram(shaderProgram);
        glBindVertexArray(VAO);
        glDrawArrays(GL_TRIANGLES, 0, 3);
        // glBindVertexArray(0); // not necessary to unbind each time, since here, only one VAO is used

        glfwSwapBuffers(window); // swap front and back buffers used during this render
        glfwPollEvents(); // check for events created by keyboard or mouse
    }

    // ======================================== release all assets =====================================
    glDeleteVertexArrays(1, &VAO);
    glDeleteBuffers(1, &VBO);
    glDeleteProgram(shaderProgram);
    
    glfwTerminate();
    return 0;
}

/* Callback function to be called by GLFW on window resize and update OpenGL Viewport */
void framebuffer_size_callback(GLFWwindow* window, int width, int height) {
    glViewport(0, 0, width, height);
}

/* 
    Check user input:
    check if user clicked ESC and if yes close the window.
 */
void processInput(GLFWwindow* window) {
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
        glfwSetWindowShouldClose(window, true);
    }
}
