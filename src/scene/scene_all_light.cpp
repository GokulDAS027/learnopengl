// clang-format off
#include <glad/glad.h>
#include <GLFW/glfw3.h>
// clang-format on

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#include <iostream>

void framebuffer_size_callback(GLFWwindow *window, int width, int height);
void mouse_callback(GLFWwindow *window, double xpos, double ypos);
void scroll_callback(GLFWwindow *window, double xoffset, double yoffset);
void processInput(GLFWwindow *window);
unsigned int loadTexture(char const *path);

const unsigned int SCR_WIDTH = 700;
const unsigned int SCR_HEIGHT = 500;

// mouse angles and position
float yaw = -90.0f, pitch = 0.0f;
float lastX = 400, lastY = 300;
bool firstMouse = true;

// camera field of view
float fov = 45.0f;

// camera
glm::vec3 cameraPos = glm::vec3(0.0f, 0.0f, 3.0f);
glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);

// for movement balance
float deltaTime = 0.0f; // Time between current frame and last frame
float lastFrame = 0.0f; // Time of last frame

// directional light position
glm::vec3 lightPos(1.0f, 0.5f, 1.0f);

// =========================================================================
// Shaders
// =========================================================================
const char *cubeVertexShaderSource = R"""(
  #version 330 core
  layout (location = 0) in vec3 aPos;
  layout (location = 1) in vec3 aNormal;
  layout (location = 2) in vec2 aTexCoords;
   
  out vec3 FragPos;
  out vec3 Normal;
  out vec2 TexCoords;
  
  uniform mat4 model;
  uniform mat4 view;
  uniform mat4 projection;
  
  void main() {
    FragPos = vec3(model * vec4(aPos, 1.0));
    Normal = mat3(transpose(inverse(model))) * aNormal;
    TexCoords = aTexCoords;
    
    gl_Position = projection * view * vec4(FragPos, 1.0);
  }
)""";

const char *cubeFragmentShaderSource = R"""(
  #version 330 core
  out vec4 FragColor;

  struct Material {
    sampler2D diffuse;
    sampler2D specular;
    float shininess;
  }; 

  struct DirLight {
    vec3 direction;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
  };

  struct PointLight {
    vec3 position;
    
    float constant;
    float linear;
    float quadratic;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
  };

  struct SpotLight {
    vec3 position;
    vec3 direction;
    float cutOff;
    float outerCutOff;
  
    float constant;
    float linear;
    float quadratic;
  
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;       
  };

  #define NR_POINT_LIGHTS 4

  in vec3 FragPos;
  in vec3 Normal;
  in vec2 TexCoords;

  uniform vec3 viewPos;
  uniform DirLight dirLight;
  uniform PointLight pointLights[NR_POINT_LIGHTS];
  uniform SpotLight spotLight;
  uniform Material material;

  // function prototypes
  vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir);
  vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir);
  vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir);

  void main() {    
    vec3 norm = normalize(Normal);
    vec3 viewDir = normalize(viewPos - FragPos);
    
    // phase 1: directional lighting
    vec3 result = CalcDirLight(dirLight, norm, viewDir);
    
    // phase 2: point lights
    for(int i = 0; i < NR_POINT_LIGHTS; i++)
        result += CalcPointLight(pointLights[i], norm, FragPos, viewDir);    
    
    // phase 3: spot light
    result += CalcSpotLight(spotLight, norm, FragPos, viewDir);
    
    FragColor = vec4(result, 1.0);
  }

  // calculates the color when using a directional light.
  vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir) {
    vec3 lightDir = normalize(-light.direction);
    
    // diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);

    // specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    
    // combine results
    vec3 ambient = light.ambient * vec3(texture(material.diffuse, TexCoords));
    vec3 diffuse = light.diffuse * diff * vec3(texture(material.diffuse, TexCoords));
    vec3 specular = light.specular * spec * vec3(texture(material.specular, TexCoords));

    return (ambient + diffuse + specular);
  }

  // calculates the color when using a point light.
  vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir) {
    vec3 lightDir = normalize(light.position - fragPos);
    
    // diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    
    // specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    
    // attenuation
    float distance = length(light.position - fragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));    
    
    // combine results
    vec3 ambient = light.ambient * vec3(texture(material.diffuse, TexCoords));
    vec3 diffuse = light.diffuse * diff * vec3(texture(material.diffuse, TexCoords));
    vec3 specular = light.specular * spec * vec3(texture(material.specular, TexCoords));
    
    ambient *= attenuation;
    diffuse *= attenuation;
    specular *= attenuation;
    
    return (ambient + diffuse + specular);
  }

  // calculates the color when using a spot light.
  vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir) {
    vec3 lightDir = normalize(light.position - fragPos);
    
    // diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    
    // specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    
    // attenuation
    float distance = length(light.position - fragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));    
    
    // spotlight intensity
    float theta = dot(lightDir, normalize(-light.direction)); 
    float epsilon = light.cutOff - light.outerCutOff;
    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
    
    // combine results
    vec3 ambient = light.ambient * vec3(texture(material.diffuse, TexCoords));
    vec3 diffuse = light.diffuse * diff * vec3(texture(material.diffuse, TexCoords));
    vec3 specular = light.specular * spec * vec3(texture(material.specular, TexCoords));
    
    ambient *= attenuation * intensity;
    diffuse *= attenuation * intensity;
    specular *= attenuation * intensity;
    
    return (ambient + diffuse + specular);
  }
)""";

const char *lightVertexShaderSource = R"""(
  #version 330 core
  layout (location = 0) in vec3 aPos;
  
  uniform mat4 model;
  uniform mat4 view;
  uniform mat4 projection;

  void main() {
    gl_Position = projection * view * model * vec4(aPos, 1.0);
  }
)""";

const char *lightFragmentShaderSource = R"""(
                                          #version 330 core
                                          out vec4 FragColor;
                                          void main() {
                                            FragColor = vec4(1.0);
                                          }
                                        )""";

int main() {
  // =========================================================================
  // GLFW
  // =========================================================================
  // init GLFW and OpenGL version and profile
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifdef __APPLE__
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // for MacOS
#endif

  // try creating window
  GLFWwindow *window =
      glfwCreateWindow(SCR_WIDTH, SCR_HEIGHT,
                       "learnopengl: 3D scene with all light", NULL, NULL);
  if (window == NULL) {
    std::cout << "GLFW: Failed to create window. \n" << std::endl;
    glfwTerminate();
    return -1;
  }
  // set newly created window as context of current thread
  glfwMakeContextCurrent(window);
  // mouse cursor config for first person movement
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  // set viewport size adjust callback
  glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
  // mouse callback
  glfwSetCursorPosCallback(window, mouse_callback);
  // scroll callback
  glfwSetScrollCallback(window, scroll_callback);

  // =========================================================================
  // GLAD
  // =========================================================================
  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
    std::cout << "Failed to initialize GLAD" << std::endl;
    return -1;
  }

  // =========================================================================
  // Shaders
  // =========================================================================
  int success;
  char infoLog[512];

  // CUBE
  unsigned int cubeVertexShader = glCreateShader(GL_VERTEX_SHADER);
  unsigned int cubeFragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  unsigned int cubeShaderProgram = glCreateProgram();

  glShaderSource(cubeVertexShader, 1, &cubeVertexShaderSource, NULL);
  glCompileShader(cubeVertexShader);
  glGetShaderiv(cubeVertexShader, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(cubeVertexShader, 512, NULL, infoLog);
    std::cout << "ERROR::CUBE_SHADER::VERTEX::COMPILATION_FAILED\n"
              << infoLog << std::endl;
  }

  glShaderSource(cubeFragmentShader, 1, &cubeFragmentShaderSource, NULL);
  glCompileShader(cubeFragmentShader);
  glGetShaderiv(cubeFragmentShader, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(cubeFragmentShader, 512, NULL, infoLog);
    std::cout << "ERROR::CUBE_SHADER::FRAGMENT::COMPILATION_FAILED\n"
              << infoLog << std::endl;
  }

  glAttachShader(cubeShaderProgram, cubeVertexShader);
  glAttachShader(cubeShaderProgram, cubeFragmentShader);
  glLinkProgram(cubeShaderProgram);
  glGetProgramiv(cubeShaderProgram, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetProgramInfoLog(cubeShaderProgram, 512, NULL, infoLog);
    std::cout << "ERROR::CUBE_SHADER_PROGRAM::LINKING_FAILED\n"
              << infoLog << std::endl;
  }

  // shader objects can be deleted after binding with cubeShaderProgram
  glDeleteShader(cubeVertexShader);
  glDeleteShader(cubeFragmentShader);

  // LIGHT SOURCE
  unsigned int lightVertexShader = glCreateShader(GL_VERTEX_SHADER);
  unsigned int lightFragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  unsigned int lightShaderProgram = glCreateProgram();

  glShaderSource(lightVertexShader, 1, &lightVertexShaderSource, NULL);
  glCompileShader(lightVertexShader);
  glGetShaderiv(lightVertexShader, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(lightVertexShader, 512, NULL, infoLog);
    std::cout << "ERROR::LIGHT_SHADER::VERTEX::COMPILATION_FAILED\n"
              << infoLog << std::endl;
  }

  glShaderSource(lightFragmentShader, 1, &lightFragmentShaderSource, NULL);
  glCompileShader(lightFragmentShader);
  glGetShaderiv(lightFragmentShader, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(lightFragmentShader, 512, NULL, infoLog);
    std::cout << "ERROR::LIGHT_SHADER::FRAGMENT::COMPILATION_FAILED\n"
              << infoLog << std::endl;
  }

  glAttachShader(lightShaderProgram, lightVertexShader);
  glAttachShader(lightShaderProgram, lightFragmentShader);
  glLinkProgram(lightShaderProgram);
  glGetProgramiv(lightShaderProgram, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetProgramInfoLog(lightShaderProgram, 512, NULL, infoLog);
    std::cout << "ERROR::LIGHT_SHADER_PROGRAM::LINKING_FAILED\n"
              << infoLog << std::endl;
  }

  // shader objects can be deleted after binding with lightShaderProgram
  glDeleteShader(lightVertexShader);
  glDeleteShader(lightFragmentShader);

  // =========================================================================
  // OpenGL Config
  // =========================================================================
  // enable z-buffer depth testing
  glEnable(GL_DEPTH_TEST);

  // =========================================================================
  // Vertices and VertexBuffers
  // =========================================================================
  // cube vertices coordinates
  float vertices[] = {
      // positions          // normals           // texture coords
      -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, //
      0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 0.0f,  //
      0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,   //
      0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 1.0f, 1.0f,   //
      -0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f,  //
      -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, 0.0f, 0.0f, //
                                                          //
      -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,   //
      0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 0.0f,    //
      0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,     //
      0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 1.0f, 1.0f,     //
      -0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 1.0f,    //
      -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f, 0.0f, 0.0f,   //
                                                          //
      -0.5f, 0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,   //
      -0.5f, 0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 1.0f,  //
      -0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, //
      -0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 1.0f, //
      -0.5f, -0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 0.0f, 0.0f,  //
      -0.5f, 0.5f, 0.5f, -1.0f, 0.0f, 0.0f, 1.0f, 0.0f,   //
                                                          //
      0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,     //
      0.5f, 0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 1.0f,    //
      0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,   //
      0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 1.0f,   //
      0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 0.0f, 0.0f,    //
      0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f, 1.0f, 0.0f,     //
                                                          //
      -0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, //
      0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 1.0f,  //
      0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,   //
      0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f,   //
      -0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 0.0f,  //
      -0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, 0.0f, 1.0f, //
                                                          //
      -0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f,   //
      0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 1.0f,    //
      0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,     //
      0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 1.0f, 0.0f,     //
      -0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 0.0f,    //
      -0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f, 0.0f, 1.0f    //
  };

  glm::vec3 cubePositions[] = {
      glm::vec3(0.0f, 0.0f, 0.0f),     //
      glm::vec3(2.0f, 5.0f, -15.0f),   //
      glm::vec3(-1.5f, -2.2f, -2.5f),  //
      glm::vec3(-3.8f, -2.0f, -12.3f), //
      glm::vec3(2.4f, -0.4f, -3.5f),   //
      glm::vec3(-1.7f, 3.0f, -7.5f),   //
      glm::vec3(1.3f, -2.0f, -2.5f),   //
      glm::vec3(1.5f, 2.0f, -2.5f),    //
      glm::vec3(1.5f, 0.2f, -1.5f),    //
      glm::vec3(-1.3f, 1.0f, -1.5f)    //
  };

  // point light positions
  glm::vec3 pointLightPositions[] = {
      glm::vec3(0.7f, 0.2f, 2.0f),    //
      glm::vec3(2.3f, -3.3f, -4.0f),  //
      glm::vec3(-4.0f, 2.0f, -12.0f), //
      glm::vec3(0.0f, 0.0f, -3.0f)    //
  };

  unsigned int cubeVAO, VBO;
  glGenVertexArrays(1, &cubeVAO);
  glGenBuffers(1, &VBO);

  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  glBindVertexArray(cubeVAO);

  // position attribute
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void *)0);
  glEnableVertexAttribArray(0);
  // normal
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void *)(3 * sizeof(float)));
  glEnableVertexAttribArray(1);
  // texture coordinates
  glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void *)(6 * sizeof(float)));
  glEnableVertexAttribArray(2);

  // light source vao and attributes
  unsigned int lightVAO;
  glGenVertexArrays(1, &lightVAO);
  glBindVertexArray(lightVAO);
  // we only need to bind to the VBO, the container's VBO's data already
  // contains the data.
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  // set the vertex attribute
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void *)0);
  glEnableVertexAttribArray(0);

  // =========================================================================
  // Lightmap
  // =========================================================================
  // diffuse lightmap
  unsigned int diffuseMap = loadTexture("../assets/wooden_container_II.png");

  // specular lightmap
  unsigned int specularMap =
      loadTexture("../assets/wooden_container_II_specular.png");

  // ======================================================
  // set constant uniforms outside game loop
  // ======================================================
  glUseProgram(cubeShaderProgram);

  // material properties -----------------------------------------
  glUniform1i(glGetUniformLocation(cubeShaderProgram, "material.diffuse"), 0);
  glUniform1i(glGetUniformLocation(cubeShaderProgram, "material.specular"), 1);
  glUniform1f(glGetUniformLocation(cubeShaderProgram, "material.shininess"), 32.0f);

  // directional light -------------------------------------------
  glm::vec3 dirLightColor = glm::vec3(1.0f, 1.0f, 1.0f);
  glm::vec3 dirLightDiffuseColor = dirLightColor * glm::vec3(0.5f);
  glm::vec3 dirLightAmbientColor = dirLightColor * glm::vec3(0.05f);
  glm::vec3 dirLightSpecularColor = dirLightColor * glm::vec3(1.0f);
  glm::vec3 dirLightDirection = glm::vec3(-0.2f, -1.0f, -0.3f);

  glUniform3fv(glGetUniformLocation(cubeShaderProgram, "dirLight.ambient"), 1, glm::value_ptr(dirLightAmbientColor));
  glUniform3fv(glGetUniformLocation(cubeShaderProgram, "dirLight.diffuse"), 1, glm::value_ptr(dirLightDiffuseColor));
  glUniform3fv(glGetUniformLocation(cubeShaderProgram, "dirLight.specular"), 1, glm::value_ptr(dirLightSpecularColor));
  glUniform3fv(glGetUniformLocation(cubeShaderProgram, "dirLight.direction"), 1, glm::value_ptr(dirLightDirection));

  // point lights -------------------------------------------------
  glm::vec3 pointLightColor = glm::vec3(1.0f, 1.0f, 1.0f);
  glm::vec3 pointLightAmbientColor = pointLightColor * glm::vec3(0.05f);
  glm::vec3 pointLightDiffuseColor = pointLightColor * glm::vec3(0.8f);
  glm::vec3 pointLightSpecularColor = pointLightColor * glm::vec3(1.0f);

  glUniform3fv(glGetUniformLocation(cubeShaderProgram, "pointLights[0].position"), 1, glm::value_ptr(pointLightPositions[0]));
  glUniform3fv(glGetUniformLocation(cubeShaderProgram, "pointLights[0].ambient"), 1, glm::value_ptr(pointLightAmbientColor));
  glUniform3fv(glGetUniformLocation(cubeShaderProgram, "pointLights[0].diffuse"), 1, glm::value_ptr(pointLightDiffuseColor));
  glUniform3fv(glGetUniformLocation(cubeShaderProgram, "pointLights[0].specular"), 1, glm::value_ptr(pointLightSpecularColor));
  glUniform1f(glGetUniformLocation(cubeShaderProgram,  "pointLights[0].constant"), 1.0f);
  glUniform1f(glGetUniformLocation(cubeShaderProgram,  "pointLights[0].linear"), 0.09f);
  glUniform1f(glGetUniformLocation(cubeShaderProgram,  "pointLights[0].quadratic"), 0.032f);

  glUniform3fv(glGetUniformLocation(cubeShaderProgram, "pointLights[1].position"), 1, glm::value_ptr(pointLightPositions[1]));
  glUniform3fv(glGetUniformLocation(cubeShaderProgram, "pointLights[1].ambient"), 1, glm::value_ptr(pointLightAmbientColor));
  glUniform3fv(glGetUniformLocation(cubeShaderProgram, "pointLights[1].diffuse"), 1, glm::value_ptr(pointLightDiffuseColor));
  glUniform3fv(glGetUniformLocation(cubeShaderProgram, "pointLights[1].specular"), 1, glm::value_ptr(pointLightSpecularColor));
  glUniform1f(glGetUniformLocation(cubeShaderProgram,  "pointLights[1].constant"), 1.0f);
  glUniform1f(glGetUniformLocation(cubeShaderProgram,  "pointLights[1].linear"), 0.09f);
  glUniform1f(glGetUniformLocation(cubeShaderProgram,  "pointLights[1].quadratic"), 0.032f);

  glUniform3fv(glGetUniformLocation(cubeShaderProgram, "pointLights[2].position"), 1, glm::value_ptr(pointLightPositions[2]));
  glUniform3fv(glGetUniformLocation(cubeShaderProgram, "pointLights[2].ambient"), 1, glm::value_ptr(pointLightAmbientColor));
  glUniform3fv(glGetUniformLocation(cubeShaderProgram, "pointLights[2].diffuse"), 1, glm::value_ptr(pointLightDiffuseColor));
  glUniform3fv(glGetUniformLocation(cubeShaderProgram, "pointLights[2].specular"), 1, glm::value_ptr(pointLightSpecularColor));
  glUniform1f(glGetUniformLocation(cubeShaderProgram,  "pointLights[2].constant"), 1.0f);
  glUniform1f(glGetUniformLocation(cubeShaderProgram,  "pointLights[2].linear"), 0.09f);
  glUniform1f(glGetUniformLocation(cubeShaderProgram,  "pointLights[2].quadratic"), 0.032f);

  glUniform3fv(glGetUniformLocation(cubeShaderProgram, "pointLights[3].position"), 1, glm::value_ptr(pointLightPositions[3]));
  glUniform3fv(glGetUniformLocation(cubeShaderProgram, "pointLights[3].ambient"), 1, glm::value_ptr(pointLightAmbientColor));
  glUniform3fv(glGetUniformLocation(cubeShaderProgram, "pointLights[3].diffuse"), 1, glm::value_ptr(pointLightDiffuseColor));
  glUniform3fv(glGetUniformLocation(cubeShaderProgram, "pointLights[3].specular"), 1, glm::value_ptr(pointLightSpecularColor));
  glUniform1f(glGetUniformLocation(cubeShaderProgram,  "pointLights[3].constant"), 1.0f);
  glUniform1f(glGetUniformLocation(cubeShaderProgram,  "pointLights[3].linear"), 0.09f);
  glUniform1f(glGetUniformLocation(cubeShaderProgram,  "pointLights[3].quadratic"), 0.032f);

  // specular ------------------------------------------------------------
  glm::vec3 spotLightColor = glm::vec3(1.0f, 1.0f, 1.0f);
  glm::vec3 spotLightAmbientColor = spotLightColor * glm::vec3(0.05f);
  glm::vec3 spotLightDiffuseColor = spotLightColor * glm::vec3(0.8f);
  glm::vec3 spotLightSpecularColor = spotLightColor * glm::vec3(1.0f);

  glUniform1f(glGetUniformLocation(cubeShaderProgram, "spotLight.cutOff"), glm::cos(glm::radians(12.5f)));
  glUniform1f(glGetUniformLocation(cubeShaderProgram, "spotLight.outerCutOff"), glm::cos(glm::radians(17.5f)));
  glUniform3fv(glGetUniformLocation(cubeShaderProgram, "spotLight.ambient"), 1, glm::value_ptr(spotLightAmbientColor));
  glUniform3fv(glGetUniformLocation(cubeShaderProgram, "spotLight.diffuse"), 1, glm::value_ptr(spotLightDiffuseColor));
  glUniform3fv(glGetUniformLocation(cubeShaderProgram, "spotLight.specular"), 1, glm::value_ptr(spotLightSpecularColor));
  glUniform1f(glGetUniformLocation(cubeShaderProgram,  "spotLight.constant"), 1.0f);
  glUniform1f(glGetUniformLocation(cubeShaderProgram,  "spotLight.linear"), 0.09f);
  glUniform1f(glGetUniformLocation(cubeShaderProgram,  "spotLight.quadratic"), 0.032f);

  // =========================================================================
  // Render Loop
  // =========================================================================
  while (!glfwWindowShouldClose(window)) {
    float currentFrame = static_cast<float>(glfwGetTime());
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    // input
    processInput(window);

    // render
    glClearColor(0.1f, 0.1f, 0.1f, 0.1f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // update projection matrix
    // fov to zoom with scroll
    glm::mat4 projection = glm::perspective(glm::radians(fov), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
    // update view matrix
    glm::mat4 view = glm::mat4(1.0f);
    view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);

    // CUBE ==================================================================
    glUseProgram(cubeShaderProgram);
            
    glUniform3fv(glGetUniformLocation(cubeShaderProgram, "viewPos"), 1, glm::value_ptr(cameraPos));
    glUniformMatrix4fv(glGetUniformLocation(cubeShaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
    glUniformMatrix4fv(glGetUniformLocation(cubeShaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));

    // specular variables
    glUniform3fv(glGetUniformLocation(cubeShaderProgram, "spotLight.position"), 1, glm::value_ptr(cameraPos));
    glUniform3fv(glGetUniformLocation(cubeShaderProgram, "spotLight.direction"), 1, glm::value_ptr(cameraFront));
    

    // bind diffuse map
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, diffuseMap);
    // bind specular map
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, specularMap);

    // render container
    glBindVertexArray(cubeVAO);

    // draw different cubes
    for (unsigned int i = 0; i < 10; i++) {
      glm::mat4 model = glm::mat4(1.0f);
      // translate
      model = glm::translate(model, cubePositions[i]);
      // rotate
      float angle = 20.0f * i;
      model = glm::rotate(model, glm::radians(angle), glm::vec3(1.0f, 0.3f, 0.5f));
      
      glUniformMatrix4fv(glGetUniformLocation(cubeShaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));

      glDrawArrays(GL_TRIANGLES, 0, 36);
    }

    // LIGHT SOURCE ==========================================================
    glUseProgram(lightShaderProgram);

    glUniformMatrix4fv(glGetUniformLocation(lightShaderProgram, "projection"), 1, GL_FALSE, glm::value_ptr(projection));
    glUniformMatrix4fv(glGetUniformLocation(lightShaderProgram, "view"), 1, GL_FALSE, glm::value_ptr(view));

    // render container
    glBindVertexArray(lightVAO);
    
    for (unsigned int i = 0; i < 4; i++) {
      glm::mat4 model = glm::mat4(1.0f);
      model = glm::translate(model, pointLightPositions[i]);
      model = glm::scale(model, glm::vec3(0.2f));
      glUniformMatrix4fv(glGetUniformLocation(lightShaderProgram, "model"), 1, GL_FALSE, glm::value_ptr(model));
      
      // draw
      glDrawArrays(GL_TRIANGLES, 0, 36);
    }

    // glfw: swap buffers and poll IO events
    // (keys pressed/released, mouse moved etc.)
    glfwSwapBuffers(window);
    glfwPollEvents();
  }
  // release all assets
  glDeleteVertexArrays(1, &cubeVAO);
  glDeleteVertexArrays(1, &lightVAO);
  glDeleteBuffers(1, &VBO);

  // glfw: terminate, clearing all previously allocated GLFW resources.
  glfwTerminate();

  return 0;
}

/*
 * Callback function to be called by GLFW on window resize and update OpenGL
 * Viewport
 */
void framebuffer_size_callback(GLFWwindow *window, int width, int height) {
  glViewport(0, 0, width, height);
}

/*
 * Callback functioin to be called on mouse movement update
 * 3D view rotation
 */
void mouse_callback(GLFWwindow *window, double xpos, double ypos) {
  // to prevent the abrupt jump
  if (firstMouse) {
    lastX = xpos;
    lastY = ypos;
    firstMouse = false;
  }

  // difference between current and last position
  float xoffset = xpos - lastX;
  float yoffset =
      lastY - ypos; // reversed since y-coordinates range from bottom to top
  lastX = xpos;
  lastY = ypos;

  // adjust mouse sensitivity
  const float sensitivity = 0.1f;
  xoffset *= sensitivity;
  yoffset *= sensitivity;

  // update angles
  yaw += xoffset;
  pitch += yoffset;

  // pitch limit
  if (pitch > 89.0f)
    pitch = 89.0f;
  if (pitch < -89.0f)
    pitch = -89.0f;

  // calculate the direction vector
  glm::vec3 direction;
  direction.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
  direction.y = sin(glm::radians(pitch));
  direction.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
  cameraFront = glm::normalize(direction);
}

/*
 * Callback functioin to be called on mouse scroll update
 * Zoom in and out
 */
void scroll_callback(GLFWwindow *window, double xoffset, double yoffset) {
  fov -= (float)yoffset;
  if (fov < 1.0f)
    fov = 1.0f;
  if (fov > 45.0f)
    fov = 45.0f;
}

/*
 * Check user input:
 * check if user clicked ESC and if yes close the window.
 */
void processInput(GLFWwindow *window) {
  // quit on pressing ESC
  if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
    glfwSetWindowShouldClose(window, true);
  }
  
  // set camera speed
  float cameraSpeed = 2.5f * deltaTime;
  if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
    cameraSpeed = 10.0f * deltaTime;
  }

  // camera movement
  if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    cameraPos += cameraSpeed * cameraFront;
  if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    cameraPos -= cameraSpeed * cameraFront;
  if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    cameraPos -=
        glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
  if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    cameraPos +=
        glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
}

/*
 * Load texture image:
 * load texture image from assets using stbi_image
 */
unsigned int loadTexture(char const *path) {
  unsigned int textureID;
  glGenTextures(1, &textureID);

  int width, height, nrComponents;
  unsigned char *data = stbi_load(path, &width, &height, &nrComponents, 0);
  if (data) {
    GLenum format;
    if (nrComponents == 1)
      format = GL_RED;
    else if (nrComponents == 3)
      format = GL_RGB;
    else if (nrComponents == 4)
      format = GL_RGBA;

    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format,
                 GL_UNSIGNED_BYTE, data);
    glGenerateMipmap(GL_TEXTURE_2D);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                    GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    stbi_image_free(data);
  } else {
    std::cout << "Texture failed to load at path: " << path << std::endl;
    stbi_image_free(data);
  }

  return textureID;
}
