// clang-format off
#include <glad/glad.h>
#include <GLFW/glfw3.h>
// clang-format on

#include "glm/ext/matrix_float4x4.hpp"
#include "glm/ext/vector_float3.hpp"
#include "glm/gtc/type_ptr.hpp"
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <iostream>

void framebuffer_size_callback(GLFWwindow *window, int width, int height);
void mouse_callback(GLFWwindow *window, double xpos, double ypos);
void scroll_callback(GLFWwindow *window, double xoffset, double yoffset);
void processInput(GLFWwindow *window);

const unsigned int SCR_WIDTH = 700;
const unsigned int SCR_HEIGHT = 500;

// mouse angles and position
float yaw = -90.0f, pitch = 0.0f;
float lastX = 400, lastY = 300;
bool firstMouse = true;

// camera field of view
float fov = 45.0f;

// camera
glm::vec3 cameraPos = glm::vec3(0.0f, 0.0f, 3.0f);
glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);

// for movement balance
float deltaTime = 0.0f; // Time between current frame and last frame
float lastFrame = 0.0f; // Time of last frame

// lighting
glm::vec3 lightPos(1.5f, 0.5f, 1.5f);

// =========================================================================
// Shaders
// =========================================================================
const char *cubeVertexShaderSource = R"""(
  #version 330 core
  layout (location = 0) in vec3 aPos;
  layout (location = 1) in vec3 aNormal;
  
  out vec3 FragPos;
  out vec3 Normal;
  
  uniform mat4 model;
  uniform mat4 view;
  uniform mat4 projection;
  
  void main() {
    gl_Position = projection * view * model * vec4(aPos, 1.0);
    FragPos = vec3(model * vec4(aPos, 1.0));
    Normal = mat3(transpose(inverse(model))) * aNormal;
  }
)""";

const char *cubeFragmentShaderSource = R"""(
  #version 330 core
  out vec4 FragColor;

  struct Material {
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;

    float shininess;
  };

  struct Light {
    vec3 position;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
  };

  in vec3 Normal;
  in vec3 FragPos;

  uniform vec3 viewPos;
  uniform Light light;
  uniform Material material;
  
  void main() {
    // ambient 
    vec3 ambient = light.ambient * material.ambient;
    
    // diffuse
    vec3 normal = normalize(Normal);
    vec3 lightDir = normalize(light.position - FragPos);
    float diff = max(dot(normal, lightDir), 0.0);
    vec3 diffuse = light.diffuse * (diff * material.diffuse);
    
    // specular
    vec3 viewDir  = normalize(viewPos - FragPos);
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(reflectDir, normal), 0), 32);
    vec3 specular = light.specular * (spec * material.specular);

    vec3 result = ambient + diffuse + specular;
    FragColor = vec4(result, 1.0);
  }
)""";

const char *lightVertexShaderSource = R"""(
  #version 330 core
  layout (location = 0) in vec3 aPos;
  
  uniform mat4 model;
  uniform mat4 view;
  uniform mat4 projection;

  void main() {
    gl_Position = projection * view * model * vec4(aPos, 1.0);
  }
)""";

const char *lightFragmentShaderSource = R"""(
                                          #version 330 core
                                          out vec4 FragColor;
                                          void main() {
                                            FragColor = vec4(1.0);
                                          }
                                        )""";

int main() {
  // =========================================================================
  // GLFW
  // =========================================================================
  // init GLFW and OpenGL version and profile
  glfwInit();
  glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
  glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
  glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#ifdef __APPLE__
  glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // for MacOS
#endif

  // try creating window
  GLFWwindow *window = glfwCreateWindow(
      SCR_WIDTH, SCR_HEIGHT, "learnopengl: 3D scene lighting", NULL, NULL);
  if (window == NULL) {
    std::cout << "GLFW: Failed to create window. \n" << std::endl;
    glfwTerminate();
    return -1;
  }
  // set newly created window as context of current thread
  glfwMakeContextCurrent(window);
  // mouse cursor config for first person movement
  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
  // set viewport size adjust callback
  glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
  // mouse callback
  glfwSetCursorPosCallback(window, mouse_callback);
  // scroll callback
  glfwSetScrollCallback(window, scroll_callback);

  // =========================================================================
  // GLAD
  // =========================================================================
  if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)) {
    std::cout << "Failed to initialize GLAD" << std::endl;
    return -1;
  }

  // =========================================================================
  // Shaders
  // =========================================================================
  int success;
  char infoLog[512];

  // CUBE
  unsigned int cubeVertexShader = glCreateShader(GL_VERTEX_SHADER);
  unsigned int cubeFragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  unsigned int cubeShaderProgram = glCreateProgram();

  glShaderSource(cubeVertexShader, 1, &cubeVertexShaderSource, NULL);
  glCompileShader(cubeVertexShader);
  glGetShaderiv(cubeVertexShader, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(cubeVertexShader, 512, NULL, infoLog);
    std::cout << "ERROR::CUBE_SHADER::VERTEX::COMPILATION_FAILED\n"
              << infoLog << std::endl;
  }

  glShaderSource(cubeFragmentShader, 1, &cubeFragmentShaderSource, NULL);
  glCompileShader(cubeFragmentShader);
  glGetShaderiv(cubeFragmentShader, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(cubeFragmentShader, 512, NULL, infoLog);
    std::cout << "ERROR::CUBE_SHADER::FRAGMENT::COMPILATION_FAILED\n"
              << infoLog << std::endl;
  }

  glAttachShader(cubeShaderProgram, cubeVertexShader);
  glAttachShader(cubeShaderProgram, cubeFragmentShader);
  glLinkProgram(cubeShaderProgram);
  glGetProgramiv(cubeShaderProgram, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetProgramInfoLog(cubeShaderProgram, 512, NULL, infoLog);
    std::cout << "ERROR::CUBE_SHADER_PROGRAM::LINKING_FAILED\n"
              << infoLog << std::endl;
  }

  // shader objects can be deleted after binding with cubeShaderProgram
  glDeleteShader(cubeVertexShader);
  glDeleteShader(cubeFragmentShader);

  // LIGHT SOURCE
  unsigned int lightVertexShader = glCreateShader(GL_VERTEX_SHADER);
  unsigned int lightFragmentShader = glCreateShader(GL_FRAGMENT_SHADER);
  unsigned int lightShaderProgram = glCreateProgram();

  glShaderSource(lightVertexShader, 1, &lightVertexShaderSource, NULL);
  glCompileShader(lightVertexShader);
  glGetShaderiv(lightVertexShader, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(lightVertexShader, 512, NULL, infoLog);
    std::cout << "ERROR::LIGHT_SHADER::VERTEX::COMPILATION_FAILED\n"
              << infoLog << std::endl;
  }

  glShaderSource(lightFragmentShader, 1, &lightFragmentShaderSource, NULL);
  glCompileShader(lightFragmentShader);
  glGetShaderiv(lightFragmentShader, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetShaderInfoLog(lightFragmentShader, 512, NULL, infoLog);
    std::cout << "ERROR::LIGHT_SHADER::FRAGMENT::COMPILATION_FAILED\n"
              << infoLog << std::endl;
  }

  glAttachShader(lightShaderProgram, lightVertexShader);
  glAttachShader(lightShaderProgram, lightFragmentShader);
  glLinkProgram(lightShaderProgram);
  glGetProgramiv(lightShaderProgram, GL_COMPILE_STATUS, &success);
  if (!success) {
    glGetProgramInfoLog(lightShaderProgram, 512, NULL, infoLog);
    std::cout << "ERROR::LIGHT_SHADER_PROGRAM::LINKING_FAILED\n"
              << infoLog << std::endl;
  }

  // shader objects can be deleted after binding with lightShaderProgram
  glDeleteShader(lightVertexShader);
  glDeleteShader(lightFragmentShader);

  // =========================================================================
  // OpenGL Config
  // =========================================================================
  // enable z-buffer depth testing
  glEnable(GL_DEPTH_TEST);

  // =========================================================================
  // Vertices and VertexBuffers
  // =========================================================================
  // cube vertices coordinates
  float vertices[] = {
      -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, //
      0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f,  //
      0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f,   //
      0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f,   //
      -0.5f, 0.5f, -0.5f, 0.0f, 0.0f, -1.0f,  //
      -0.5f, -0.5f, -0.5f, 0.0f, 0.0f, -1.0f, //
                                              //
      -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f,   //
      0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f,    //
      0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f,     //
      0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f,     //
      -0.5f, 0.5f, 0.5f, 0.0f, 0.0f, 1.0f,    //
      -0.5f, -0.5f, 0.5f, 0.0f, 0.0f, 1.0f,   //
                                              //
      -0.5f, 0.5f, 0.5f, -1.0f, 0.0f, 0.0f,   //
      -0.5f, 0.5f, -0.5f, -1.0f, 0.0f, 0.0f,  //
      -0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f, //
      -0.5f, -0.5f, -0.5f, -1.0f, 0.0f, 0.0f, //
      -0.5f, -0.5f, 0.5f, -1.0f, 0.0f, 0.0f,  //
      -0.5f, 0.5f, 0.5f, -1.0f, 0.0f, 0.0f,   //
                                              //
      0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f,     //
      0.5f, 0.5f, -0.5f, 1.0f, 0.0f, 0.0f,    //
      0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f,   //
      0.5f, -0.5f, -0.5f, 1.0f, 0.0f, 0.0f,   //
      0.5f, -0.5f, 0.5f, 1.0f, 0.0f, 0.0f,    //
      0.5f, 0.5f, 0.5f, 1.0f, 0.0f, 0.0f,     //
                                              //
      -0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, //
      0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f,  //
      0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f,   //
      0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f,   //
      -0.5f, -0.5f, 0.5f, 0.0f, -1.0f, 0.0f,  //
      -0.5f, -0.5f, -0.5f, 0.0f, -1.0f, 0.0f, //
                                              //
      -0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f,   //
      0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f,    //
      0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f,     //
      0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f,     //
      -0.5f, 0.5f, 0.5f, 0.0f, 1.0f, 0.0f,    //
      -0.5f, 0.5f, -0.5f, 0.0f, 1.0f, 0.0f    //
  };

  unsigned int cubeVAO, VBO;
  glGenVertexArrays(1, &cubeVAO);
  glGenBuffers(1, &VBO);

  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

  glBindVertexArray(cubeVAO);

  // position attribute
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void *)0);
  glEnableVertexAttribArray(0);
  // normal
  glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float),
                        (void *)(3 * sizeof(float)));
  glEnableVertexAttribArray(1);

  // light source vao and attributes
  unsigned int lightVAO;
  glGenVertexArrays(1, &lightVAO);
  glBindVertexArray(lightVAO);
  // we only need to bind to the VBO, the container's VBO's data already
  // contains the data.
  glBindBuffer(GL_ARRAY_BUFFER, VBO);
  // set the vertex attribute
  glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 6 * sizeof(float), (void *)0);
  glEnableVertexAttribArray(0);
  // =========================================================================
  // set uniform outside gameloop
  // =========================================================================
  // light location to calculate diffuse and specular lighting
  glUseProgram(cubeShaderProgram);
  int lightPosUniformLoc =
      glGetUniformLocation(cubeShaderProgram, "light.position");
  glUniform3fv(lightPosUniformLoc, 1, glm::value_ptr(lightPos));

  // =========================================================================
  // Render Loop
  // =========================================================================
  while (!glfwWindowShouldClose(window)) {
    float currentFrame = static_cast<float>(glfwGetTime());
    deltaTime = currentFrame - lastFrame;
    lastFrame = currentFrame;

    // input
    processInput(window);

    // render
    glClearColor(0.1f, 0.1f, 0.1f, 0.1f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    // CUBE ==================================================================
    glUseProgram(cubeShaderProgram);

    // fragment shader uniform -----------------------------------------------
    // light properties
    glm::vec3 lightColor;
    lightColor.x = static_cast<float>(sin(glfwGetTime() * 2.0));
    lightColor.y = static_cast<float>(sin(glfwGetTime() * 0.7));
    lightColor.z = static_cast<float>(sin(glfwGetTime() * 1.3));
    glm::vec3 diffuseColor =
        lightColor * glm::vec3(0.5f); // decrease the influence
    glm::vec3 ambientColor = diffuseColor * glm::vec3(0.2f); // low influence

    int lightAmbientUnifromLoc =
        glGetUniformLocation(cubeShaderProgram, "light.ambient");
    glUniform3fv(lightAmbientUnifromLoc, 1, glm::value_ptr(ambientColor));

    int lightDiffuseUniformLoc =
        glGetUniformLocation(cubeShaderProgram, "light.diffuse");
    glUniform3fv(lightDiffuseUniformLoc, 1, glm::value_ptr(diffuseColor));

    int lightSpecularUniformLoc =
        glGetUniformLocation(cubeShaderProgram, "light.specular");
    glUniform3fv(lightSpecularUniformLoc, 1,
                 glm::value_ptr(glm::vec3(1.0f, 1.0f, 1.0f)));

    int materialAmbientUnifromLoc =
        glGetUniformLocation(cubeShaderProgram, "material.ambient");
    glUniform3fv(materialAmbientUnifromLoc, 1,
                 glm::value_ptr(glm::vec3(1.0f, 0.5f, 0.31f)));

    int materialDiffuseUniformLoc =
        glGetUniformLocation(cubeShaderProgram, "material.diffuse");
    glUniform3fv(materialDiffuseUniformLoc, 1,
                 glm::value_ptr(glm::vec3(1.0f, 0.5f, 0.31f)));

    int materialSpecularUniformLoc =
        glGetUniformLocation(cubeShaderProgram, "material.specular");
    glUniform3fv(materialSpecularUniformLoc, 1,
                 glm::value_ptr(glm::vec3(0.5f, 0.5f, 0.5f)));

    int materialShininessUniformLoc =
        glGetUniformLocation(cubeShaderProgram, "material.shining");
    glUniform1f(materialShininessUniformLoc, 32.0f);

    // camera position to calculate specular lighting
    int cameraPosUniformLoc =
        glGetUniformLocation(cubeShaderProgram, "viewPos");
    glUniform3fv(cameraPosUniformLoc, 1, glm::value_ptr(cameraPos));

    // vertex shader uniform -------------------------------------------------
    // update projection matrix
    // fov to zoom with scroll
    glm::mat4 projection = glm::perspective(
        glm::radians(fov), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
    int projectionUniformLoc =
        glGetUniformLocation(cubeShaderProgram, "projection");
    glUniformMatrix4fv(projectionUniformLoc, 1, GL_FALSE,
                       glm::value_ptr(projection));

    // update view matrix
    // camera position
    glm::mat4 view = glm::mat4(1.0f);
    view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
    int viewUniformLoc = glGetUniformLocation(cubeShaderProgram, "view");
    glUniformMatrix4fv(viewUniformLoc, 1, GL_FALSE, glm::value_ptr(view));

    glm::mat4 model = glm::mat4(1.0f);
    int modelUniformLoc = glGetUniformLocation(cubeShaderProgram, "model");
    glUniformMatrix4fv(modelUniformLoc, 1, GL_FALSE, glm::value_ptr(model));

    // render container
    glBindVertexArray(cubeVAO);
    // draw
    glDrawArrays(GL_TRIANGLES, 0, 36);

    // LIGHT SOURCE ==========================================================
    glUseProgram(lightShaderProgram);

    projectionUniformLoc =
        glGetUniformLocation(lightShaderProgram, "projection");
    glUniformMatrix4fv(projectionUniformLoc, 1, GL_FALSE,
                       glm::value_ptr(projection));

    viewUniformLoc = glGetUniformLocation(lightShaderProgram, "view");
    glUniformMatrix4fv(viewUniformLoc, 1, GL_FALSE, glm::value_ptr(view));

    model = glm::mat4(1.0f);
    model = glm::translate(model, lightPos);
    model = glm::scale(model, glm::vec3(0.2f)); // a smaller cube
    modelUniformLoc = glGetUniformLocation(lightShaderProgram, "model");
    glUniformMatrix4fv(modelUniformLoc, 1, GL_FALSE, glm::value_ptr(model));

    // render container
    glBindVertexArray(lightVAO);
    // draw
    glDrawArrays(GL_TRIANGLES, 0, 36);

    // glfw: swap buffers and poll IO events
    // (keys pressed/released, mouse moved etc.)
    glfwSwapBuffers(window);
    glfwPollEvents();
  }
  // release all assets
  glDeleteVertexArrays(1, &cubeVAO);
  glDeleteVertexArrays(1, &lightVAO);
  glDeleteBuffers(1, &VBO);

  // glfw: terminate, clearing all previously allocated GLFW resources.
  glfwTerminate();

  return 0;
}

/*
 * Callback function to be called by GLFW on window resize and update OpenGL
 * Viewport
 */
void framebuffer_size_callback(GLFWwindow *window, int width, int height) {
  glViewport(0, 0, width, height);
}

/*
 * Callback functioin to be called on mouse movement update
 * 3D view rotation
 */
void mouse_callback(GLFWwindow *window, double xpos, double ypos) {
  // to prevent the abrupt jump
  if (firstMouse) {
    lastX = xpos;
    lastY = ypos;
    firstMouse = false;
  }

  // difference between current and last position
  float xoffset = xpos - lastX;
  float yoffset =
      lastY - ypos; // reversed since y-coordinates range from bottom to top
  lastX = xpos;
  lastY = ypos;

  // adjust mouse sensitivity
  const float sensitivity = 0.1f;
  xoffset *= sensitivity;
  yoffset *= sensitivity;

  // update angles
  yaw += xoffset;
  pitch += yoffset;

  // pitch limit
  if (pitch > 89.0f)
    pitch = 89.0f;
  if (pitch < -89.0f)
    pitch = -89.0f;

  // calculate the direction vector
  glm::vec3 direction;
  direction.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
  direction.y = sin(glm::radians(pitch));
  direction.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
  cameraFront = glm::normalize(direction);
}

/*
 * Callback functioin to be called on mouse scroll update
 * Zoom in and out
 */
void scroll_callback(GLFWwindow *window, double xoffset, double yoffset) {
  fov -= (float)yoffset;
  if (fov < 1.0f)
    fov = 1.0f;
  if (fov > 45.0f)
    fov = 45.0f;
}

/*
    Check user input:
    check if user clicked ESC and if yes close the window.
 */
void processInput(GLFWwindow *window) {
  // quit on pressing ESC
  if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS) {
    glfwSetWindowShouldClose(window, true);
  }

  // camera movement
  const float cameraSpeed = 2.5f * deltaTime;
  if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
    cameraPos += cameraSpeed * cameraFront;
  if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
    cameraPos -= cameraSpeed * cameraFront;
  if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
    cameraPos -=
        glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
  if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
    cameraPos +=
        glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
}
